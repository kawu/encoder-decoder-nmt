Neural RNN encoder-decoder translation system
=============================================

A toy machine translation system based on a simple variant of the RNN
encoder-decoder architecture.  A more detailed description of the model can be
found in [this document][specs].


Installation
------------

The code relies on Python 3.8+.  If you use conda, you can set up an
appropriate environment using the following commands (substituting `<env-name>`
for the name of the environment):
```bash
conda create --name <env-name> python=3.8
conda activate <env-name>
```
Then, to install the project's dependencies:
```bash
pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html
```


Sample dataset
--------------

The repository already contains a small [sample parallel dataset](sample)
extracted from the [tokenize data][multi30k-tok] in the
[multi30k][multi30k-repo] repository.  You can re-create it using the
`create_sample.py` script, but first you might need to update the `multi30k`
git submodule.
```bash
git submodule update --init
python create_sample.py
```
The `create_sample.py` script contains variable `N` which can be modified to
change the size of the resulting sample.  You can in particular set it to some
large number (e.g. `10**6`) to extract the entire `multi30k` tokenized
dataset.


Running
-------

The main script can be found in `main.py` and you can run it using:
```bash
python -i main.py
```
The `-i` flag allows to interactively inspect the model, translate example
sentences, etc., after the model is trained.


Structure
---------

Modules:
* `core.py` -- core types (word, input, output, ...)
* `multi30k.py` -- parsing/reading [multi30k/dataset files][multi30k-tok]
* `encoder.py` -- generic Encoder for object <-> int conversion
* `preprocessing.py` -- source/target sentence preprocessing, word <-> int conversion
* `modules.py` -- PyTorch modules for RNN encoding, RNN decoding, and translation
* `train.py` -- generic training procedure

Scripts:
* `create_sample.py` -- script for [sample dataset creation](#sample-dataset)
* `main.py` -- main script which combines data extraction, preprocessing, model creation, training


[specs]: https://user.phil.hhu.de/~waszczuk/teaching/hhu-dl-wi20/session11/u9_eng.pdf
[multi30k-repo]: https://github.com/multi30k/dataset
[multi30k-tok]: https://github.com/multi30k/dataset/tree/master/data/task1/tok
