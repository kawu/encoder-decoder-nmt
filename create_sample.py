''''Script to create a sample, toy dataset based on the multi30k/dataset.'''

import multi30k

# Size of the sample
N = 10**2

if __name__ == '__main__':
    # Read the entire training dataset
    en_path = "multi30k/data/task1/tok/train.lc.norm.tok.en"
    de_path = "multi30k/data/task1/tok/train.lc.norm.tok.de"
    data = multi30k.read_parallel(en_path, de_path)
    # Extract the shortest N dataset pairs
    shortestN = sorted(data,
        key=lambda xs: max(len(xs[0]), len(xs[1]))
    )[:N]
    # Write the parallel dataset to the sample directory
    multi30k.write_parallel(shortestN,
        "sample/sample.en",
        "sample/sample.de",
    )