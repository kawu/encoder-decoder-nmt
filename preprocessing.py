from typing import NewType, List, Tuple, Optional, Iterator, Dict, Iterable

from collections import Counter

from torch import Tensor
import torch

from encoder import Encoder
from core import *

##################################################
# Special symbols
##################################################

# Special end-of-sentence marker
END = "<END>"

# Special unknown-word marker (NOTE: currently not used, but
# to scale up the model to larger datasets it may be useful
# to replace rare words with UNK during pre-processing)
UNK = "<UNK>"

#################################################
# Basic preprocessing
#################################################

def preprocess_inp(xs: Inp) -> Inp:
    '''Preprocess an input sentence to a form expected by
    a translation model.'''
    # NOTE: Appending END at the beginning of input sentences
    # is not a crucial step, but it might be helpful (especially
    # if the model were extended with ,,attention'')
    return xs + [END]

def preprocess_out(xs: Out) -> Out:
    '''Preprocess an output sentence to a form expected by
    a translation model.'''
    # NOTE: The translation model assumes that each sentence ends
    # with the special END marker, hence the following step is
    # necessary.  Note however that we could alternatively change
    # the model to add the END marker at the end of each input sentence,
    # instead of doing that during pre-processing, which would be more
    # elegant but possibly slightly more difficult to achieve.
    return xs + [END]

def preprocess(data: List[Tuple[Inp, Out]]) -> List[Tuple[Inp, Out]]:
    '''Preprocess an entire input/output dataset.'''
    return [(preprocess_inp(xs), preprocess_out(ys)) for xs, ys in data]

#################################################
# Word <-> int encoding
#################################################

def create_encoders(
    data: List[Tuple[Inp, Out]]
) -> Tuple[Encoder[Word], Encoder[Word]]:
    '''Create a pair of word <-> int encoders, for input and output words,
    respectively.'''
    src_enc = Encoder(word for inp, _ in data for word in inp)
    trg_enc = Encoder(word for _, out in data for word in out)
    return (src_enc, trg_enc)

def encode_sent(sent: List[Word], enc: Encoder[Word]) -> Tensor:
    '''Encode a sentence with a given word <-> int encoder.

    Parameters
    ----------
    sent : List[Word]
        List of words
    enc : Encoder[Word]
        Word <-> int conversion object

    Returns
    -------
    Tensor[N]
        Vector tensor of length N, where N is the length of the `sent`
        parameter; each value in the vector belongs to [0..V], where
        V is the size of the vocabulary, i.e., the size of `enc`
    '''
    return torch.tensor([enc.encode(word) for word in sent])

def encode_with(
    data: List[Tuple[Inp, Out]],
    src_enc: Encoder[Word],
    trg_enc: Encoder[Word]
) -> List[Tuple[EncInp, EncOut]]:
    '''Encode a dataset using given source and target word encoders.'''
    return [
        ( encode_sent(inp, src_enc)
        , encode_sent(out, trg_enc)
        )
        for inp, out in data
    ]