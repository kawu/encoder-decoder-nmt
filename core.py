from typing import List
from torch import Tensor


# Word form (in source or target language) or special symbol (END, UNK)
Word = str

# Source (input) sentence: list of words
Inp = List[Word]

# Target (output) sentence: list of words
Out = List[Word]

# Encoded input: 1d vector of target word indices
EncInp = Tensor

# Encoded output: 1d vector of target word indices
EncOut = Tensor