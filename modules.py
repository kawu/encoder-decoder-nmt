from typing import List

import torch
import torch.nn as nn

from torch import Tensor

import encoder
from core import Word
import preprocessing as pre


class Encoder(nn.Module):
    '''RNN encoder takes a source sentence on input an encodes it to
    a fixed-size tensor representation.

    Type: Tensor[N x D] -> Tuple[Tensor[H], Tensor[H]]

      where:

        * N is the length of the input sentence
        * D is the embedding size on input
        * H is the hidden size on output

    I.e., the encoder returns a pair of tensors each of shape H, which
    together summarize the input sentence.  See `forward` for more details.
    '''

    def __init__(self, emb_size: int, out_size: int):
        super().__init__()
        # Initial ,,hidden state''
        self.h0 = nn.Parameter(torch.randn(out_size).unsqueeze(0))
        # Initial ,,cell state''
        self.c0 = nn.Parameter(torch.randn(out_size).unsqueeze(0))
        # LSTM computation cell
        self.cell = nn.LSTMCell(input_size=emb_size, hidden_size=out_size)

    def forward(self, xs):
        '''Apply the LSTM encoder to an input sequence.

        Parameters
        ----------
        xs : Tensor[N x D]
            Tensor of shape N x D, where N is the input sequence length
            and D is the embedding size

        Returns
        -------
        h_n, c_n : Tuple[Tensor[H], Tensor[H]]
            A pair of tensors each of shape H, where H is the hidden output
            size.  More concretely, h_n and c_n is the last hidden and cell
            state of the underlying LSTM, respectively.
        '''
        # Initial hidden and cell states
        h, c = self.h0, self.c0
        for x in xs:
            # Compute the new hidden and cell states
            h, c = self.cell(x.unsqueeze(0), (h, c))
        # Return the resulting state (tuple of hidden and cell state);
        # `squeeze` is used to ged rid of the redundant batch dimension.
        return (h.squeeze(0), c.squeeze(0))


class Decoder(nn.Module):
    '''RNN decoder serves to unfold the vector representation produced
    by the encoder to a sequence of words.

    Type: Tuple[Tensor[H], Tensor[H], Tensor[M]] -> Tensor[M x T]

    where:

    * The input contains:
        * (h_n, c_n) provided by the Encoder
        * Vector representing the target words
    * The output is a matrix of scores for each position in the target sentence
        * M is the length of the output sentence
        * T is the size of the target vocabulary

    See `forward` for more details.
    '''

    def __init__(self, inp_size: int, emb_size: int, vocab_size: int, end_marker: int):
        '''
        Parameters
        ----------
        inp_size : int
            Size of h_n (and c_n) provided by the encoder; also size of
            the hidden/cell state of the decoding RNN
        emb_size : int
            Size of the embeddings of the target vocabulary
        vocab_size : int
            Size of the target vocabulary
        end_marker : int
            End-of-sentence symbol; must be within the vocabulary
            (0 <= end_marker < vocab_size)
        '''
        super().__init__()
        # Make sure the end-of-sentence symbol is within the vocabulary
        assert 0 <= end_marker < vocab_size
        self.end_marker = end_marker
        self.vocab_size = vocab_size
        # Embedding module for the target language
        self.emb = nn.Embedding(
            num_embeddings=vocab_size+1,
            embedding_dim=emb_size,
            padding_idx=vocab_size    
        )
        # LSTM computation cell
        self.lstm_cell = nn.LSTMCell(input_size=emb_size+inp_size*2, hidden_size=inp_size)
        # Parameters for the scoring/softmax formula
        self.U_o = nn.Parameter(torch.randn(vocab_size, inp_size))
        self.V_o = nn.Parameter(torch.randn(vocab_size, emb_size))
        self.W_o = nn.Parameter(torch.randn(vocab_size, inp_size*2))

    def step(self, h_n, c_n, h, c, y):
        '''Calculate the scores for the next word prediction.

        Parameters
        ----------
        h_n, c_n : Tensor[H], Tensor[H]
            Last hidden and cell state of the encoder (both of size H)
        h, c : Tensor[H], Tensor[H]
            Current hidden and cell state of the LSTM decoder
        y : scalar int tensor
            Previous word index

        Returns
        -------
        scores : Tensor[T]
            Vector of unnormalized scores, one score per word in the
            target vocabulary, where T is the target vocabulary size.
        '''
        # Embed the last predicted word
        assert 0 <= y <= self.vocab_size
        y_emb = self.emb(y)

        # Construct the input vector for the LSTM cell
        x = torch.cat((y_emb, h_n, c_n))
        # Run the LSTM computation cell
        h, c = self.lstm_cell(x.unsqueeze(0), (h, c))

        # Compute the scores
        scores = (self.U_o @ h.squeeze(0)) + \
                 (self.V_o @ y_emb) + \
                 (self.W_o @ torch.cat((h_n, c_n)))

        # Return the scores and the new decoder state
        return h, c, scores

    def forward(self, h_n: Tensor, c_n: Tensor, ys: Tensor):
        '''Calculate the scores the model assignes to the individual
        target words in `ys` given the input summary `(h_n, c_n)`.

        Parameters
        ----------
        h_n, c_n : Tensor[H], Tensor[H]
            Last hidden and cell state of the encoder (both of size H)
        ys : Tensor[M]
            Vector of target word indices of length M

        Returns
        -------
        scores : Tensor[M, T]
            Matrix of scores for each output word.  Each row in the matrix
            is a vector of unnormalized scores, one score per word in the
            target vocabulary, where T is the target vocabulary size.
        '''
        # Verify the assumption that the last element in `ys` is the
        # (encoded) end-of-sentence marker
        assert ys[-1] == self.end_marker
        # Initial hidden and cell states
        h = h_n.unsqueeze(0)
        c = c_n.unsqueeze(0)
        # Output: sequence of score tensors for the individual target words
        output_scores = []
        # We use the end-of-sentence symbol as beginning-of-sentence, too
        # (we can be sure it's distinct from all actual word indices).
        # We also discard the end-of-sentence marker at the end, because
        # once we reach it we are done, there's no need to predict the
        # subsequent word.
        for y in [ys[-1]] + list(ys[:-1]):
            h, c, scores = self.step(h_n, c_n, h, c, y)
            output_scores.append(scores)
        return torch.stack(output_scores)

    def decode(self, h_n: Tensor, c_n: Tensor, max_iter: int = 1000) -> Tensor:
        '''Generate a sequence of target word indices for a given representation
        of input sentence (h_n, c_n).

        Parameters
        ----------
        h_n, c_n : Tensor[H], Tensor[H]
            Last hidden and cell state of the encoder (both of size H)
        max_iter : int
            Maximum number of iterations

        Returns
        -------
        ys : Tensor[_]
            Vector of generated target word indices; its length is not known
            in advance but can be restricted using the `max_iter` parameter.
        '''
        # Initial hidden and cell states
        h = h_n.unsqueeze(0)
        c = c_n.unsqueeze(0)
        # Output sequence of target word indices
        ys: List[Tensor] = []
        # Last predicted word (initialized to the end marker)
        y = torch.tensor(self.end_marker)
        # Iteration counter
        k = 0
        # Generate at most `max_iter` words
        while k < max_iter:
            # Perform the decoding step
            h, c, scores = self.step(h_n, c_n, h, c, y)
            # Predict the next word
            y = torch.argmax(scores)
            # Break the loop in case the end marker is predicted
            if y == self.end_marker:
                break
            # Append the predicted word on the output list
            ys.append(y)
            # Increase the iteration counter
            k += 1
        # Stack and return the predicted word indices
        return torch.stack(ys)


class Translator(nn.Module):
    '''Encoder/decoder-based translation module.'''

    def __init__(self,
        src_enc: encoder.Encoder[Word],
        trg_enc: encoder.Encoder[Word],
        emb_size: int,
        hid_size: int,
        end_marker: str
    ):
        '''
        Parameters
        ----------
        src_enc : Encoder[Word] (not RNN Encoder)
            Word <-> int encoding object for the source language
        trg_enc : Encoder[Word] (not RNN Encoder)
            Word <-> int encoding object for the target language
        emb_size : int
            Size of the source and target word embeddings
        hid_size : int
            Size of both (a) the RNN encoder and (b) the RNN decoder
            hidden/cell states
        end_marker : str
            Special end-of-sentence marker symbol; must make part of
            the target language vocabulary (i.e., can be converted to
            int with `trg_enc`)
        '''
        super().__init__()
        # Store the word<->int encoding objects
        self.src_enc = src_enc
        self.trg_enc = trg_enc
        src_vocab_size = src_enc.size()
        trg_vocab_size = trg_enc.size()
        # Create the embedding module for the source language (the embedding
        # for the target language is included in the Decoder)
        self.emb = nn.Embedding(
            num_embeddings=src_vocab_size+1,
            embedding_dim=emb_size,
            padding_idx=src_vocab_size
        )
        # Create the RNN encoder and the RNN decoder
        self.enc = Encoder(emb_size, hid_size)
        self.dec = Decoder(
            inp_size=hid_size,
            emb_size=emb_size,
            vocab_size=trg_vocab_size,
            end_marker=trg_enc.encode(end_marker)
        )

    def forward(self, xs, ys):
        '''Apply the encoder/decoder combination to the source/target
        sentence pair.

        Parameters
        ----------
        xs : Tensor[N]
            Vector of source (input) word indices of length N
        ys : Tensor[M]
            Vector of target (output) word indices of length M

        Returns
        -------
        scores : Tensor[M, T]
            Matrix of scores for each output word.  See the documentation
            of the `forward` method of the `Decoder`.
        '''
        h_n, c_n = self.enc(self.emb(xs))
        return self.dec(h_n, c_n, ys)

    @torch.no_grad()
    def translate(self, sent: List[Word]) -> List[Word]:
        '''Translate a source sentence to a target sentence using
        the underlying encoder/decoder combination.

        Parameters
        ----------
        sent : List[Word]
            List of words in the source (input) sentence

        Returns
        -------
        List[Word]
            List of generated target words
        '''
        # Pre-process the input sentence
        sent = pre.preprocess_inp(sent)
        # Embed the source sentence
        xs = self.emb(pre.encode_sent(sent, self.src_enc))
        # Apply the RNN encoder
        h_n, c_n = self.enc(xs)
        # MT-decode the target sentence from the source sentence representation
        trg_enc = self.dec.decode(h_n, c_n)
        # Decode to strings the resulting target word indices
        return [self.trg_enc.decode(y.item()) for y in trg_enc]