import torch
import torch.nn as nn

import random

import preprocessing as pre
import multi30k
from train import train
from modules import Translator

# To make data-split reproducible
random.seed(0)

# Read and preprocess the sample dataset
en_path = "sample/sample.en"
de_path = "sample/sample.de"
all_data = pre.preprocess(multi30k.read_parallel(en_path, de_path))

# Shuffle and split the dataset (NOTE: normally you would also
# want to dedicate some part of the dataset for the final
# evaluation)
split_point = int(len(all_data) * 0.9)
random.shuffle(all_data)
train_data = all_data[:split_point]
dev_data = all_data[split_point:]

# Create word <-> int encoders for source and target words
src_enc, trg_enc = pre.create_encoders(train_data)

# Encode the datasets as tensors
enc_train = pre.encode_with(train_data, src_enc, trg_enc)
enc_dev = pre.encode_with(dev_data, src_enc, trg_enc)

# Report data size
print("# Train size =", len(enc_train))
print("# Dev size =", len(enc_dev))

# Report other stats
print("# Source vocabulary size =", src_enc.size())
print("# Target vocabulary size =", trg_enc.size())

# Create the model
model = Translator(
    src_enc=src_enc,
    trg_enc=trg_enc,
    emb_size=100,
    hid_size=300,
    end_marker=pre.END
)

def accuracy(model, data):
    """Calculate the accuracy of the model on the given dataset
    of (encoded) input/output pairs.

    NOTE: This is not true accuracy, since the model receives on input
    the output indices, too.  Besides accuracy is not a good measure of
    translation quality anyway.
    """
    correct, total = 0, 0
    for x, y in data:
        pred_y = torch.argmax(model(x, y), dim=1)
        correct += (y == pred_y).sum()
        total += len(y)
    return float(correct) / total

def cross_entropy_loss(model, x, y_gold):
    '''Cross entropy loss between the output of the `model`
    on input `x` and the target output `y_gold`.
    '''
    loss = nn.CrossEntropyLoss()
    # NOTE: The target output is provided on input of the model
    # due to the nature of the MT task (see the specs)
    y_scores = model(x, y_gold)
    return loss(y_scores, y_gold)


# Train the model
train(model, enc_train, enc_dev, cross_entropy_loss, accuracy, epoch_num=10, learning_rate=0.001, report_rate=1)