''''Module for parsing/reading multi30k/dataset files.

This module provides functionality for parsing multi30k/dataset files,
in particular the tokenized files that can be found in:

    https://github.com/multi30k/dataset/tree/master/data/task1/tok
'''


from typing import List, Tuple, Iterable


def handle_html(raw_sent: str) -> str:
    """Handle special HTML symbols present in multi30k files."""
    repl_map = [
        ('&quot;', '"'),
        ('&apos;', "'"),
        # The one below is probably an error in the dataset?
        ('&amp; amp ;', '&'),
        ('&amp;', '&')
    ]
    for x, y in repl_map:
        raw_sent = raw_sent.replace(x, y)
    return raw_sent

def read_mono(path) -> List[List[str]]:
    """Read the list of tokenized sentences from a given file."""
    data = []
    with open(path, "r", encoding="utf-8") as data_file:
        for raw_line in data_file:
            data.append(handle_html(raw_line).split())
    return data

def read_parallel(src_path, trg_path) -> List[Tuple[List[str], List[str]]]:
    """Read the parallel dataset stored in the given files."""
    src_data = read_mono(src_path)
    trg_data = read_mono(trg_path)
    assert len(src_data) == len(trg_data)
    return list(zip(src_data, trg_data))

def write_mono(data: Iterable[List[str]], out_path):
    """Write a dataset to a file."""
    with open(out_path, "w", encoding="utf-8") as data_file:
        for sent in data:
            data_file.write(' '.join(sent) + '\n')

def write_parallel(data: List[Tuple[List[str], List[str]]], src_path, trg_path):
    """Write a parallel dataset to given files."""
    write_mono((x for x, _ in data), src_path)
    write_mono((y for _, y in data), trg_path)


# Example usage:
if __name__ == "__main__":
    en_path = "multi30k/data/task1/tok/test_2016_flickr.lc.norm.tok.en"
    de_path = "multi30k/data/task1/tok/test_2016_flickr.lc.norm.tok.de"
    data = read_parallel(en_path, de_path)